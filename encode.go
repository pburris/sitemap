package sitemap

import (
	"bufio"
	"bytes"
	"encoding/xml"
	"io"
)

const xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9"

type loc struct {
	Value string `xml:"loc"`
}

type urlset struct {
	Urls  []loc  `xml:"url"`
	Xmlns string `xml:"xmlns,attr"`
}

func toXml(urls []string) ([]byte, error) {
	set := urlset{
		Xmlns: xmlns,
	}
	var ret bytes.Buffer
	w := bufio.NewWriter(&ret)

	for _, u := range urls {
		if u != "" {
			set.Urls = append(set.Urls, loc{u})
		}
	}

	io.WriteString(w, xml.Header)
	enc := xml.NewEncoder(w)
	enc.Indent("", "  ")
	if err := enc.Encode(set); err != nil {
		return nil, err
	}
	io.WriteString(w, "\n")

	return ret.Bytes(), nil
}
