# Sitemap builder

A sitemap builder written in Go.


### Usage

Download and install:
`go get github.com/jumballaya/sitemap && cd $GOPATH/src/github.com/jumballaya/sitemap && go install`

Flags:
```
Usage of sitemap:
  -depth int
    	maximum depth to look for links (default 3)
  -output string
    	file to output the xml sitemap (default "sitemap.xml")
  -site string
    	the website you want to generate a sitemap for (default "http://localhost")
```
