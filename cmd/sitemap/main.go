package main

import (
	"flag"
	"io/ioutil"

	"github.com/jumballaya/sitemap"
)

func main() {
	site := flag.String("site", "http://localhost", "the website you want to generate a sitemap for")
	depth := flag.Int("depth", 3, "maximum depth to look for links")
	output := flag.String("output", "sitemap.xml", "file to output the xml sitemap")
	flag.Parse()

	xml, err := sitemap.Generate(*site, *depth)
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile(*output, xml, 0600)
	if err != nil {
		panic(err)
	}
}
