package link

import (
	"bytes"
	"io"
	"strings"

	"golang.org/x/net/html"
)

// Link represents a link in an HTML document (<a href="...">)
type Link struct {
	Href string `json:"href" xml:"url"`
	Text string `json:"text" xml:"loc"`
}

// Parse will take in an HTML document and will return
// a slice of links parsed from it
func Parse(r io.Reader) ([]Link, error) {
	doc, err := html.Parse(r)
	if err != nil {
		return nil, err
	}

	var ret []Link
	nodes := linkNodes(doc)

	for _, n := range nodes {
		ret = append(ret, buildLink(n))
	}

	return ret, nil
}

func buildLink(n *html.Node) Link {
	var ret Link

	for _, attr := range n.Attr {
		if attr.Key == "href" {
			ret.Href = attr.Val
			break
		}
	}

	ret.Text = getLinkText(n)

	return ret
}

func getLinkText(n *html.Node) string {
	if n.Type == html.TextNode {
		return n.Data
	}
	if n.Type != html.ElementNode {
		return ""
	}

	var ret bytes.Buffer

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		ret.WriteString(getLinkText(c) + " ")
	}

	return strings.Join(strings.Fields(ret.String()), " ")
}

func linkNodes(n *html.Node) []*html.Node {
	if n.Type == html.ElementNode && n.Data == "a" {
		return []*html.Node{n}
	}
	var ret []*html.Node

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		ret = append(ret, linkNodes(c)...)
	}

	return ret
}
