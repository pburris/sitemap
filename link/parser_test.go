package link

import (
	"io/ioutil"
	"strings"
	"testing"
)

type parseTest struct {
	count    int
	html     string
	expected []Link
}

func openHTML(t *testing.T, path string) string {
	t.Helper()
	b, err := ioutil.ReadFile(path)
	if err != nil {
		t.Error(err)
	}
	return string(b)
}

func TestParser(t *testing.T) {
	tests := []parseTest{
		{
			html:  openHTML(t, "ex1.html"),
			count: 1,
			expected: []Link{
				{Href: "/other-page", Text: "A link to another page"},
			},
		},
		{
			html:  openHTML(t, "ex2.html"),
			count: 2,
			expected: []Link{
				{Href: "https://www.twitter.com/joncalhoun", Text: "Check me out on twitter"},
				{Href: "https://github.com/gophercises", Text: "Gophercises is on Github !"},
			},
		},
		{
			html:  openHTML(t, "ex3.html"),
			count: 3,
			expected: []Link{
				{Href: "#", Text: "Login"},
				{Href: "/lost", Text: "Lost? Need help?"},
				{Href: "https://twitter.com/marcusolsson", Text: "@marcusolsson"},
			},
		},
		{
			html:  openHTML(t, "ex4.html"),
			count: 1,
			expected: []Link{
				{Href: "/dog-cat", Text: "dog cat"},
			},
		},
	}

	for _, tt := range tests {
		reader := strings.NewReader(tt.html)
		links, err := Parse(reader)
		if err != nil {
			t.Errorf("%v", err)
		}

		if len(links) != tt.count {
			t.Errorf("wrong amount of links parsed, expected: %d, got: %d", tt.count, len(links))
		}

		for i, link := range links {
			if link.Href != tt.expected[i].Href {
				t.Errorf("error with parser, expected: %s, got: %s", tt.expected[i].Href, links[i].Href)
			}

			if link.Text != tt.expected[i].Text {
				t.Errorf("error with parser, expected: %s, got: %s", tt.expected[i].Text, links[i].Text)
			}
		}

	}
}
