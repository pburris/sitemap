package sitemap

import (
	"io"
	"net/http"
	"net/url"
	"strings"

	"github.com/jumballaya/sitemap/link"
)

type predicateFn func(string) bool
type empty struct{}

func get(uri string) ([]string, error) {
	res, err := http.Get(uri)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	reqUrl := res.Request.URL
	baseUrl := &url.URL{
		Scheme: reqUrl.Scheme,
		Host:   reqUrl.Host,
	}
	base := baseUrl.String()
	links, err := hrefs(res.Body, base)
	if err != nil {
		return nil, err
	}
	return filter(links, withPrefix(base)), nil
}

func hrefs(r io.Reader, base string) ([]string, error) {
	links, err := link.Parse(r)
	if err != nil {
		return nil, err
	}

	var ret []string
	for _, l := range links {
		switch {
		case strings.HasPrefix(l.Href, "/"):
			ret = append(ret, base+l.Href)
		case strings.HasPrefix(l.Href, "http"):
			ret = append(ret, l.Href)
		}
	}
	return ret, nil
}

func filter(links []string, predicate ...predicateFn) []string {
	var ret []string

	for _, l := range links {
		t := true
		for _, p := range predicate {
			t = t && p(l)
		}
		if t {
			ret = append(ret, l)
		}
	}

	return ret
}

func withPrefix(pfx string) predicateFn {
	return func(l string) bool {
		return strings.HasPrefix(l, pfx)
	}
}

func bfs(uri string, depth int) ([]string, error) {
	seen := make(map[string]empty)
	var q map[string]empty
	nq := map[string]empty{
		uri: empty{},
	}

	for i := 0; i <= depth; i++ {
		q, nq = nq, make(map[string]empty)
		if len(q) == 0 {
			break
		}
		for url, _ := range q {
			if _, ok := seen[url]; ok {
				continue
			}
			seen[url] = empty{}
			links, err := get(url)
			if err != nil {
				return nil, err
			}
			for _, l := range links {
				if _, ok := seen[l]; !ok {
					nq[l] = empty{}
				}
			}
		}
	}

	ret := make([]string, 0, len(seen))
	for u, _ := range seen {
		ret = append(ret, u)
	}
	return ret, nil
}

func Generate(site string, depth int) ([]byte, error) {
	/**
	 * 1. GET webpage
	 * 2. Parse the links on the page
	 * 3. Build the proper URLs with our links
	 * 4. Filter out any links w/ a different domain
	 * 5. Find all the pages (BFS)
	 * 6. Print out XML
	 */
	pages, err := bfs(site, depth)
	if err != nil {
		return nil, err
	}

	data, err := toXml(pages)
	if err != nil {
		return nil, err
	}

	return data, nil
}
